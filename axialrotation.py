# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
 joint: 2, 
      axial rotation, 
         1,                           # node 1 label 
            null,                     # relative offset (null = the motor is at the static node)
            hinge, euler, 0., 0., 0.  # relative orientation
         2,                           # node 2 label (dynamic node)
            0.025, 0., 0., # relative offset (to the dynamic node)
            hinge, euler, 0., 0., 0.  # relative orientation
         ramp, 2.*pi, 0., 1., 0.;     # angular velocity  
 
'''
#from FreeCAD import Units

import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Axialrotation:
    def __init__(self, obj, label, static, dynamic):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Get the cylinder's center of mass, so that the joint is placed here: 
        x = static.position_X - dynamic.position_X
        y = static.position_Y - dynamic.position_Y
        z = static.position_Z - dynamic.position_Z
                 
        obj.addExtension("App::GroupExtensionPython", self)          
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Axial Rotation","label",1).label = label
        obj.addProperty("App::PropertyString","joint","Axial Rotation","joint",1).joint = 'axial rotation'
        obj.addProperty("App::PropertyString","static node","Axial Rotation","static node",1).static_node = static.label
        obj.addProperty("App::PropertyString","dynamic node","Axial Rotation","dynami cnode",1).dynamic_node = dynamic.label        
        obj.addProperty("App::PropertyString","relative offset","Axial Rotation","relative offset",1).relative_offset = 'null'
        
        obj.addProperty("App::PropertyString","relative offset 1 X","Relative offset 1","relative offset 1 X",1).relative_offset_1_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Y","Relative offset 1","relative offset 1 Y",1).relative_offset_1_Y = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset 1 Z","Relative offset 1","relative offset 1 Z",1).relative_offset_1_Z = str(round(x.getValueAs('m').Value,precission))+" m"
        
        obj.addProperty("App::PropertyString","orientation matrix 1","Revolute hinge","orientation matrix 1",1).orientation_matrix_1 = "3, 0.0, 0.0, 1.0, 2, guess"
        obj.addProperty("App::PropertyString","orientation matrix 2","Revolute hinge","orientation matrix 2",1).orientation_matrix_2 = "3, 0.0, 0.0, 1.0, 2, guess"
        
        #Drive function:
        obj.addProperty("App::PropertyEnumeration","angular velocity","Drive caller","angular velocity")
        obj.angular_velocity=['const, <const_coef>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',                              
                              'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>',
                              'direct',
                              'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>',
                              'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>',
                              'double step, <initial_time>, <final_time>, <step_value>, <initial_value>',
                              'linear, <const_coef>, <slope_coef>',
                              'ramp, <slope>, <initial_time>, forever, <initial_value>',
                              'ramp, <slope>, <initial_time>, <final_time>, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',
                              'step, <initial_time>, <step_value>, <initial_value>',
                              'string',
                              'time',
                              'timestep',
                              'unit']
        
        
        obj.addProperty("App::PropertyString","a_slope","Drive caller parameters","a_slope").a_slope = '0.1'
        obj.addProperty("App::PropertyString","string","Drive caller parameters","string").string = '1.5*sin(2.*Time)'        
        obj.addProperty("App::PropertyString","slope","Drive caller parameters","slope").slope = '0.1'
        obj.addProperty("App::PropertyString","slope_coef","Drive caller parameters","slope_coef").slope_coef = '0.1'
        obj.addProperty("App::PropertyString","d_slope","Drive caller parameters","d_slope").d_slope = '0.1'
        obj.addProperty("App::PropertyString","a_initial_time","Drive caller parameters","a_initial_time").a_initial_time = '0.'
        obj.addProperty("App::PropertyString","d_initial_time","Drive caller parameters","d_initial_time").d_initial_time = '1.'
        obj.addProperty("App::PropertyString","a_final_time","Drive caller parameters","a_final_time").a_final_time = '1.'
        obj.addProperty("App::PropertyString","d_final_time","Drive caller parameters","d_final_time").d_final_time = '2.'        
        obj.addProperty("App::PropertyString","const_coef","Drive caller parameters","const_coef").const_coef = '10.'
        obj.addProperty("App::PropertyString","step_value","Drive caller parameters","step_value").step_value = '10.'
        obj.addProperty("App::PropertyString","linear_coef","Drive caller parameters","linear_coef").linear_coef = '1.'
        obj.addProperty("App::PropertyString","parabolic_coef","Drive caller parameters","parabolic_coef").parabolic_coef = '1.'
        obj.addProperty("App::PropertyString","cubic_coef","Drive caller parameters","cubic_coef").cubic_coef = '1.'        
        obj.addProperty("App::PropertyString","initial_time","Drive caller parameters","initial_time").initial_time = '0.'
        obj.addProperty("App::PropertyString","final_time","Drive caller parameters","final_time").final_time = '1.'
        obj.addProperty("App::PropertyString","angular_vel","Drive caller parameters","angular_vel").angular_vel = '10.'
        obj.addProperty("App::PropertyString","amplitude","Drive caller parameters","amplitude").amplitude = '10.'
        obj.addProperty("App::PropertyString","initial_value","Drive caller parameters","initial_value").initial_value = '0.'
        obj.addProperty("App::PropertyString","number_of_cycles","Drive caller parameters","number_of_cycles").number_of_cycles = '1.'
        
                        
        obj.addProperty("App::PropertyString","absolute position X","Initial absolute position","absolute position X",1).absolute_position_X = str(round(static.position_X.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","Initial absolute position","absolute position Y",1).absolute_position_Y = str(round(static.position_Y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","Initial absolute position","absolute position Z",1).absolute_position_Z = str(round(static.position_Z.getValueAs('m').Value,precission))+" m"
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyString","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = '1'

        obj.Proxy = self   
        
        x = static.position_X
        y = static.position_Y 
        z = static.position_Z
        
        length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + static.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 2*length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm' 
        l.ViewObject.Selectable = False             
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.ViewObject.Selectable = False
        d.Label = "jf: "+ label                    
                       
        FreeCAD.ActiveDocument.recompute()    

    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        
        ##############################################################################Calculate the new orientation: 
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:
        fp.orientation_matrix_1 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                
        fp.orientation_matrix_2 = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                       
        
        ##############################################################################Recalculate the offset, in case any of the two nodes was moved: 
        #get the pin position (the pin position is not expected to change):
        x = float(fp.absolute_position_X.split(" ")[0])  
        y = float(fp.absolute_position_Y.split(" ")[0])
        z = float(fp.absolute_position_Z.split(" ")[0])
        
        #get the node´s position:
        dynamci_node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.dynamic_node)[0]
        
        #Re-calculate the joint possition relative to it's nodes (relative offset)        
        x1 = x - dynamci_node.position_X.getValueAs("m").Value
        y1 = y - dynamci_node.position_Y.getValueAs("m").Value
        z1 = z - dynamci_node.position_Z.getValueAs("m").Value

        #Update the offset:
        fp.relative_offset_1_X = str(round(x1,precission))+" m"
        fp.relative_offset_1_Y = str(round(y1,precission))+" m"
        fp.relative_offset_1_Z = str(round(z1,precission))+" m"
        
        FreeCAD.Console.PrintMessage("AXIAL ROTATION JOINT: " +fp.label+" successful recomputation...\n") 
      