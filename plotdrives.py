# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
 
import FreeCAD
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np

#///////////////////////////////////////////////THIS CLASS PLOTS ANY DRIVE ASOCIATED TO A JOINT OR A FORCE
class PlotDrives:  
    def __init__(self, objeto):
        InitialTime = float(FreeCAD.ActiveDocument.getObject("MBDyn").initial_time[:-2])
        FinalTime = float(FreeCAD.ActiveDocument.getObject("MBDyn").final_time[:-2])
        #Retrieve the data from the .mov file 
        #Precission variable is used to round the numbers, to save ram memory when plotting large simulations
        #precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        self.time = np.linspace(InitialTime,FinalTime, int((FinalTime-InitialTime)*1000) ) 
        aux = ""
        aux1 = ""

        if (objeto.Label.startswith('force: ')):
            aux = objeto.force_value
            aux1 = 'N'

        if (objeto.Label.startswith('joint: ') and objeto.joint == "axial rotation"):
            aux = objeto.angular_velocity 
            aux1 = 'rad/s'

        if (aux == 'const, <const_coef>'):
            y = np.full(int((FinalTime-InitialTime)*1000), objeto.const_coef)
            plt.plot(self.time,y,label='const, <const_coef>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': const, <const_coef>')
            plt.show()
            
        if (aux == 'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>'):
            vfun = np.vectorize(self.cosine_half)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>')
            plt.show()

        if (aux == 'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>'):
            vfun = np.vectorize(self.cosine_one)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>')
            plt.show()

        if (aux == 'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>'):
            vfun = np.vectorize(self.cosine_forever)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>')
            plt.show()

        if (aux == 'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>'):
            vfun = np.vectorize(self.cosine_number_of_cycles)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>')
            plt.show()

        if (aux == 'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>'):
            vfun = np.vectorize(self.cubic)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>')
            plt.show()

        if (aux == 'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>'):
            vfun = np.vectorize(self.double_ramp_forever)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>')
            plt.show()
            
        if (aux == 'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>'):
            vfun = np.vectorize(self.double_ramp)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>')
            plt.show()            

        if (aux == 'double step, <initial_time>, <final_time>, <step_value>, <initial_value>'):
            vfun = np.vectorize(self.double_step)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='double step, <initial_time>, <final_time>, <step_value>, <initial_value>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': double step, <initial_time>, <final_time>, <step_value>, <initial_value>')
            plt.show()  

        if (aux == 'linear, <const_coef>, <slope_coef>'):
            vfun = np.vectorize(self.linear)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='linear, <const_coef>, <slope_coef>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': linear, <const_coef>, <slope_coef>')
            plt.show()  

        if (aux == 'ramp, <slope>, <initial_time>, forever, <initial_value>'):
            vfun = np.vectorize(self.ramp_forever)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='ramp, <slope>, <initial_time>, forever, <initial_value>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': ramp, <slope>, <initial_time>, forever, <initial_value>')
            plt.show() 

        if (aux == 'ramp, <slope>, <initial_time>, <final_time>, <initial_value>'):
            vfun = np.vectorize(self.ramp)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='ramp, <slope>, <initial_time>, <final_time>, <initial_value>', color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': ramp, <slope>, <initial_time>, <final_time>, <initial_value>')
            plt.show() 

        if (aux == 'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>'):
            vfun = np.vectorize(self.sine_half)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>')
            plt.show()

        if (aux == 'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>'):
            vfun = np.vectorize(self.sine_one)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>')
            plt.show()

        if (aux == 'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>'):
            vfun = np.vectorize(self.sine_forever)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>')
            plt.show()

        if (aux == 'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>'):
            vfun = np.vectorize(self.sine_number_of_cycles)
            y = vfun(self.time, objeto)
            plt.plot(self.time,y,label='sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',color='red')
            plt.ylabel(aux1)
            plt.xlabel('t[s]')
            #plt.legend()            
            plt.grid()
            plt.suptitle(str(objeto.Label) + ': sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>')
            plt.show()
            
#DEFINE THE FUNCTIONS:

##############################################################################################################cosine    
    def cosine_half (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if (float(objeto.initial_time) < t) and (t < float(objeto.initial_time) + np.pi/float(objeto.angular_vel)):
            return float(objeto.initial_value) + float(objeto.amplitude) * (1 - np.cos(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

        if float(objeto.initial_time) + np.pi/float(objeto.angular_vel) <= t:
            return float(objeto.initial_value) + 2 * float(objeto.amplitude)                
    
    def cosine_one (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if (float(objeto.initial_time) < t) and (t < (float(objeto.initial_time) + 2 * np.pi/float(objeto.angular_vel))):
            return float(objeto.initial_value) + float(objeto.amplitude) * (1 - np.cos(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

        if float(objeto.initial_time) + 2 * np.pi/float(objeto.angular_vel) <= t:
            return float(objeto.initial_value) + 0                  
    
    def cosine_forever (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if float(objeto.initial_time) < t:
            return float(objeto.initial_value) + float(objeto.amplitude) * (1 - np.cos(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

    def cosine_number_of_cycles (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if (float(objeto.initial_time) < t) and (t < (float(objeto.initial_time) + 2* float(objeto.number_of_cycles) * np.pi/float(objeto.angular_vel))):
            return float(objeto.initial_value) + float(objeto.amplitude) * (1 - np.cos(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

        if float(objeto.initial_time) + 2* float(objeto.number_of_cycles) * np.pi/float(objeto.angular_vel) <= t:
            return float(objeto.initial_value) + 0     

##############################################################################################################cubic            
    def cubic (self, t, objeto): 
        return float(objeto.const_coef) + float(objeto.linear_coef) * t + float(objeto.parabolic_coef) * t**2 + float(objeto.cubic_coef) * t**3
        
##############################################################################################################double step        
    def double_step (self, t, objeto):
        if t < float(objeto.initial_time):
            return float(objeto.initial_value) + 0    
        
        if float(objeto.initial_time) <= t and t <= float(objeto.final_time):
            return float(objeto.initial_value) + float(objeto.step_value) 
        
        if float(objeto.final_time) < t:
            return float(objeto.initial_value) + 0  

##############################################################################################################linear        
    def linear (self, t, objeto):
        return float(objeto.const_coef) + float(objeto.slope_coef) * t    

##############################################################################################################ramp:

    def double_ramp_forever (self, t, objeto):#double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value> 
        if t < float(objeto.a_initial_time):
            return float(objeto.initial_value) + 0        
        
        if float(objeto.a_initial_time) <= t and t <= float(objeto.a_final_time):
            return float(objeto.initial_value) + (float(objeto.a_slope) * (t - float(objeto.a_initial_time)))
        
        if float(objeto.a_initial_time) < t and t < float(objeto.d_initial_time):
            return float(objeto.initial_value) + (float(objeto.a_slope) * (float(objeto.a_final_time) - float(objeto.a_initial_time)))
        
        if float(objeto.d_initial_time) <= t and t <= float(objeto.d_final_time):
            return float(objeto.initial_value) + (float(objeto.a_slope) * (float(objeto.a_final_time) - float(objeto.a_initial_time))) + float(objeto.d_slope) * (t - float(objeto.d_initial_time))
        
        if float(objeto.d_final_time) < t:
            return float(objeto.initial_value) + (float(objeto.a_slope) * (float(objeto.a_final_time) - float(objeto.a_initial_time))) + float(objeto.d_slope) * (float(objeto.d_final_time) - float(objeto.d_initial_time))    
    
    def double_ramp (self, t, objeto):#double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>
        if t < float(objeto.a_initial_time):
            return float(objeto.initial_value) + 0        
        
        if float(objeto.a_initial_time) <= t and t <= float(objeto.a_final_time):
            return float(objeto.initial_value) + (float(objeto.a_slope) * (t - float(objeto.a_initial_time)))
        
        if float(objeto.a_initial_time) < t and t < float(objeto.d_initial_time):
            return float(objeto.initial_value) + (float(objeto.a_slope) * (float(objeto.a_final_time) - float(objeto.a_initial_time)))
        
        if float(objeto.d_initial_time) <= t and t <= float(objeto.d_final_time):
            return float(objeto.initial_value) + (float(objeto.a_slope) * (float(objeto.a_final_time) - float(objeto.a_initial_time))) + float(objeto.d_slope) * (t - float(objeto.d_initial_time))
        
        if float(objeto.d_final_time) < t:
            return float(objeto.initial_value) + (float(objeto.a_slope) * (float(objeto.a_final_time) - float(objeto.a_initial_time))) + float(objeto.d_slope) * (float(objeto.d_final_time) - float(objeto.d_initial_time))

    def ramp_forever (self, t, objeto):
        if t < float(objeto.initial_time):
            return float(objeto.initial_value) + 0    
        
        if float(objeto.initial_time) <= t and t <= float(objeto.final_time):
            return float(objeto.initial_value) + (float(objeto.slope) * (t - float(objeto.initial_time)))
        
        if float(objeto.final_time) < t:
            return float(objeto.initial_value) + (float(objeto.slope) * (float(objeto.final_time) - float(objeto.initial_time)))       
    
    def ramp (self, t, objeto):#ramp, <slope>, <initial_time>, <final_time>, <initial_value>
        if t < float(objeto.initial_time):
            return float(objeto.initial_value) + 0    
        
        if float(objeto.initial_time) <= t and t <= float(objeto.final_time):
            return float(objeto.initial_value) + (float(objeto.slope) * (t - float(objeto.initial_time)))
        
        if float(objeto.final_time) < t:
            return float(objeto.initial_value) + 0   
        
#############################################################################################################sine:
    def sine_half (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if (float(objeto.initial_time) < t) and (t < float(objeto.initial_time) + np.pi/ 2 * float(objeto.angular_vel)):
            return float(objeto.initial_value) + float(objeto.amplitude) * (np.sin(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

        if float(objeto.initial_time) + np.pi/ 2 * float(objeto.angular_vel) <= t:
            return float(objeto.initial_value) + float(objeto.amplitude)                
    
    def sine_one (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if (float(objeto.initial_time) < t) and (t < (float(objeto.initial_time) + np.pi/ 2 * float(objeto.angular_vel))):
            return float(objeto.initial_value) + float(objeto.amplitude) * (np.sin(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

        if float(objeto.initial_time) + np.pi/ 2 *float(objeto.angular_vel) <= t:
            return float(objeto.initial_value) + 0                  
    
    def sine_forever (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if float(objeto.initial_time) < t:
            return float(objeto.initial_value) + float(objeto.amplitude) * (np.sin(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

    def sine_number_of_cycles (self, t, objeto): 
        if t <= float(objeto.initial_time):
            return float(objeto.initial_value) + 0
        
        if (float(objeto.initial_time) < t) and (t < (float(objeto.initial_time) + 2* float(objeto.number_of_cycles) * np.pi/float(objeto.angular_vel))):
            return float(objeto.initial_value) + float(objeto.amplitude) * (np.sin(float(objeto.angular_vel) * (t - float(objeto.initial_time))) )     

        if float(objeto.initial_time) + 2* float(objeto.number_of_cycles) * np.pi/float(objeto.angular_vel) <= t:
            return float(objeto.initial_value) + 0     
        