# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
https://www.sky-engin.jp/en/MBDynTutorial/chap14/chap14.html
This joint constrains a node only allowing rotation about one axis. The basic syntax for the statement that defines a revolute pin is as follows.

The syntax is: 

joint: <label>, 
       revolute pin, 
       <node>, 
       <relative offset>, 
       hinge, 
       <relative orientation matrix>,
       <absolute pin position>, 
       hinge, 
       <absolute pin orientation matrix>;

<label>: an integer number to identify the joint, eg: 1,2,3... 
<node>: the label of the structural node to which the joint is attached, eg: 1,2,3... 
<relative offset>: the possition of the joint relative to it's structural node. 
For the example in the above web page is '-0.5, 0.0, 0.0', because the node is at '0.5, 0.0, 0.0' relative to the absolute origin
<relative orientation matrix>:
<absolute pin position>: pin possition relative to the absolute reference frame. It is the center of mass of the FreeCAD cylinder.
 
'''

import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Revolutepin:
    def __init__(self, obj, label, node, cylinder):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Get the cylinder's center of mass, so that the joint is placed here:
        x = FreeCAD.Units.Quantity(cylinder.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))  
        y = FreeCAD.Units.Quantity(cylinder.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))  
        z = FreeCAD.Units.Quantity(cylinder.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))  
        
        #Calculate the joint possition relative to it's node (relative offset)        
        x1 = x-node.position_X
        y1 = y-node.position_Y
        z1 = z-node.position_Z
                
        obj.addExtension("App::GroupExtensionPython", self) 
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Revolute pin","label",1).label = label
        obj.addProperty("App::PropertyString","node","Revolute pin","node",1).node = node.label
        obj.addProperty("App::PropertyString","joint","Revolute pin","joint",1).joint = 'revolute pin'
        
        #pin possition relative to it's node:
        obj.addProperty("App::PropertyString","relative offset X","Relative offset","relative offset X",1).relative_offset_X = str(round(x1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset Y","Relative offset","relative offset Y",1).relative_offset_Y = str(round(y1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset Z","Relative offset","relative offset Z",1).relative_offset_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
        
        #Absolute pin position:                
        obj.addProperty("App::PropertyString","absolute position X","Absolute pin position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","Absolute pin position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","Absolute pin position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
        
        #Rotation axis orientation:            
        obj.addProperty("App::PropertyString","orientation matrix","Revolute pin","orientation matrix",1).orientation_matrix = "3, 0.0, 0.0, 1.0, 2, guess"
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyString","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = '1'
        
        obj.Proxy = self
        
        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        length = (cylinder.Shape.BoundBox.XLength+cylinder.Shape.BoundBox.YLength+cylinder.Shape.BoundBox.ZLength)/6 # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)
        p2 = FreeCAD.Vector(0, 0, 2*length)
        #Create the rotation axis:
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/30)+' mm' 
        #l.ViewObject.Selectable = False              
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label   
        #d.ViewObject.Selectable = False 

        FreeCAD.ActiveDocument.recompute()        
        
    def execute(self, fp):
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        
        ##############################################################################Calculate the new orientation:            
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:
        fp.orientation_matrix = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                
            
        ##############################################################################Recalculate the offset, in case the node was moved: 
        #get the pin position (the pin position is not expected to change):
        x = float(fp.absolute_position_X.split(" ")[0])  
        y = float(fp.absolute_position_Y.split(" ")[0])
        z = float(fp.absolute_position_Z.split(" ")[0])
        
        #get the node´s position:
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node)[0]
        
        #Re-calculate the joint possition relative to it's node (relative offset)        
        x1 = x - node.position_X.getValueAs("m").Value
        y1 = y - node.position_Y.getValueAs("m").Value
        z1 = z - node.position_Z.getValueAs("m").Value

        #Update the offset:
        fp.relative_offset_X = str(round(x1,precission))+" m"
        fp.relative_offset_Y = str(round(y1,precission))+" m"
        fp.relative_offset_Z = str(round(z1,precission))+" m"
        
        FreeCAD.Console.PrintMessage("REVOLUTE PIN JOINT: " +fp.label+" successful recomputation...\n")
            