# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
from sympy import Point3D, Line3D
import Draft

class StructuralForce:
    def __init__(self, obj, label, node):
     
        obj.addExtension("App::GroupExtensionPython", self)  
        
        #Calculate the absolute position:       
        x = node.position_X
        y = node.position_Y
        z = node.position_Z

        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Structural force","label",1).label = label
        obj.addProperty("App::PropertyString","force","Structural force","force",1).force = 'structural force'
        obj.addProperty("App::PropertyString","node","Structural force","node",1).node = node.label
        
        
        #obj.addProperty("App::PropertyString","type","Structural force","type").type = 'absolute'
        obj.addProperty("App::PropertyEnumeration","type","Structural force","type")
        obj.type=['absolute','follower']
        
        
        obj.addProperty("App::PropertyString","position","Structural force","position",1).position = 'null'

        #Force parameters        

        obj.addProperty("App::PropertyEnumeration","force type","Force parameters","force type")
        obj.force_type=['single']

        obj.addProperty("App::PropertyString","direction","Force parameters","direction").direction = '0, 0, 1'
        
        
        #Drive function:
        obj.addProperty("App::PropertyEnumeration","force value","Drive caller","force value")
        obj.force_value=['const, <const_coef>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                              'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',                              
                              'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>',
                              'direct',
                              'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>',
                              'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>',
                              'double step, <initial_time>, <final_time>, <step_value>, <initial_value>',
                              'linear, <const_coef>, <slope_coef>',
                              'ramp, <slope>, <initial_time>, forever, <initial_value>',
                              'ramp, <slope>, <initial_time>, <final_time>, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                              'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',
                              'step, <initial_time>, <step_value>, <initial_value>',
                              'string',
                              'time',
                              'timestep',
                              'unit']
        
        
        obj.addProperty("App::PropertyString","a_slope","Drive caller parameters","a_slope").a_slope = '0.1'
        obj.addProperty("App::PropertyString","string","Drive caller parameters","string").string = '1.5*sin(2.*Time)'        
        obj.addProperty("App::PropertyString","slope","Drive caller parameters","slope").slope = '0.1'
        obj.addProperty("App::PropertyString","slope_coef","Drive caller parameters","slope_coef").slope_coef = '0.1'
        obj.addProperty("App::PropertyString","d_slope","Drive caller parameters","d_slope").d_slope = '0.1'
        obj.addProperty("App::PropertyString","a_initial_time","Drive caller parameters","a_initial_time").a_initial_time = '0.'
        obj.addProperty("App::PropertyString","d_initial_time","Drive caller parameters","d_initial_time").d_initial_time = '1.'
        obj.addProperty("App::PropertyString","a_final_time","Drive caller parameters","a_final_time").a_final_time = '1.'
        obj.addProperty("App::PropertyString","d_final_time","Drive caller parameters","d_final_time").d_final_time = '2.'        
        obj.addProperty("App::PropertyString","const_coef","Drive caller parameters","const_coef").const_coef = '10.'
        obj.addProperty("App::PropertyString","step_value","Drive caller parameters","step_value").step_value = '10.'
        obj.addProperty("App::PropertyString","linear_coef","Drive caller parameters","linear_coef").linear_coef = '1.'
        obj.addProperty("App::PropertyString","parabolic_coef","Drive caller parameters","parabolic_coef").parabolic_coef = '1.'
        obj.addProperty("App::PropertyString","cubic_coef","Drive caller parameters","cubic_coef").cubic_coef = '1.'        
        obj.addProperty("App::PropertyString","initial_time","Drive caller parameters","initial_time").initial_time = '0.'
        obj.addProperty("App::PropertyString","final_time","Drive caller parameters","final_time").final_time = '1.'
        obj.addProperty("App::PropertyString","angular_vel","Drive caller parameters","angular_vel").angular_vel = '10.'
        obj.addProperty("App::PropertyString","amplitude","Drive caller parameters","amplitude").amplitude = '10.'
        obj.addProperty("App::PropertyString","initial_value","Drive caller parameters","initial_value").initial_value = '0.'
        obj.addProperty("App::PropertyString","number_of_cycles","Drive caller parameters","number_of_cycles").number_of_cycles = '1.'

        obj.Proxy = self          

        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        length = FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4
        p1 = FreeCAD.Vector(0, 0, 0)
        #Add Z vector of the coordinate system:
        p2 = FreeCAD.Vector(length, 0, 0)
        p2 = FreeCAD.Vector(0, 0, length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: force: '+ label
        l.ViewObject.ArrowType = u"Arrow"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/30)+' mm' 
        #l.ViewObject.Selectable = False        
        
    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        
        ##############################################################################Calculate the new orientation: 
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: force: "+fp.label)[0]#get the force´s Z line        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix (it has to be a unitary vector, so that it does not alter the force magnitude):
        ar = [l1.direction[0], l1.direction[1], l1.direction[2]]    
        fp.direction = str(round(l1.direction[0]/max(ar),precission)) +", "+ str(round(l1.direction[1]/max(ar),precission)) +", "+ str(round(l1.direction[2]/max(ar),precission))
        
        #Change the placement of the foce´s Z line (in the case it´s node has been moved):
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node)[0]
        #Calculate the absolute position:       
        x = node.position_X
        y = node.position_Y
        z = node.position_Z
        ZZ.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        

        FreeCAD.Console.PrintMessage("STRUCTURAL FORCE: " +fp.label+" successful recomputation...\n")